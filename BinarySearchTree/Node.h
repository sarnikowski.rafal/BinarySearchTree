#pragma once
#include <iostream>
#include <memory>
#include <functional>

namespace BST {

	template<typename T, typename Compare>
	class Node
	{
	public:
		using node_sh = std::shared_ptr<Node<T, Compare>>;

		//warto�� przechowywana w w�le
		T value;
		//lewe dziecko
		node_sh left;
		//prawe dziecko
		node_sh right;

		//rodzic
		node_sh parent;

	public:

		//przeci��one operatory z podan� funkcj� por�wnuj�c� Compare
		friend bool operator<(const Node &lhs, const Node &rhs)
		{
			Compare compare = Compare();
			return compare(lhs.value, rhs.value);
		}

		friend bool operator>(const Node &lhs, const Node &rhs)
		{
			return rhs < lhs;
		}

		friend bool operator<=(const Node &lhs, const Node &rhs)
		{
			return !(lhs > rhs);
		}

		friend bool operator>=(const Node &lhs, const Node &rhs)
		{
			return !(lhs < rhs);
		}

		friend bool operator<(const T &lhs, const Node &rhs)
		{
			Compare compare = Compare();
			return compare(lhs, rhs.value);
		}

		friend bool operator>(const T &lhs, const Node &rhs)
		{
			return rhs < lhs;
		}

		friend bool operator<=(const T &lhs, const Node &rhs)
		{
			return !(lhs > rhs);
		}

		friend bool operator>=(const T &lhs, const Node &rhs)
		{
			return !(lhs < rhs);
		}

		Node(void)
		{
		}

		Node(T value)
		{
			this->value = value;
		}

		Node(T value, node_sh parent)
		{
			this->value = value;
			this->parent = parent;
		}
	};

}

